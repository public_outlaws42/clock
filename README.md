# Clock

![Clock Display](static/clock.png)

This was intended for a clock that will be run on a 5" LCD touchscreen display connected to a `Raspberry Pi Zero`. Right now it is displayed over HDMI from the Pi. 
It has 2 soft buttons one that switches between 12 and 24 hour clock and one that closes the clock. 

A `Raspberry Pi Zero` is a bit of overkill for a clock I suppose but i had an extra RP Zero laying around so I put it to good use.  

## Working on
I am fixing bugs as well as typos. Spelling and grammer is NOT my strong suit. :)

---

## Latest Updates
Added the ability to have status messages. The first time `clock.py` is run it will create a file `support/status.yaml` it will add one message for Christmas. Messages can be added to the list one per day in this format.
`date: message` so an example would be `12/25: Merry Christmas`. At this time I am not limiting the length of the message but obviously if you have to long of a message it is not going to look good.  The message will appear right below the date 

---

## House Keeping

### Base System

Being that this is targeted as running on the Raspberry pi all the commands will be targeted towards a Debian base system. This is what the offical Raspberry pi OS runs. 

### Editor
I specify editing the files with `nano` below because it is the easiest to use for beginners but use what you like. To save and exit in nano `Ctrl+o`  then enter to confirm saving the file. `Ctrl+x` to exit the editor

### Terminal(Raspberry pi)

Many times i will have you type or copy & paste a command into the terminal. 
In the Raspberry pi or Linux generally for that matter you can start a terminal with `ctrl+alt+t` keyboard shortcut. This is assuming you are working directly on the Raspberry pi. Being you probably would not have a keyboard and such connected the Raspberry pi. I suggest connecting remotely. Make sure you have the raspberry pi connected to your network. Then you will use `ssh` to connect. You can read this [article](https://www.tomshardware.com/how-to/use-ssh-connect-to-remote-computer) on how to connect using `ssh` if you don't already know.

---

## Prerequisites

- `Python 3.8` at a minimum - In Linux this should not be a problem most recent distros come with at least python 3.  
- `tkinter` This could already be installed with python, but I have had instances were it was not. 
- `pyyaml` - You will need `pyyaml` to read the yaml config file. 

---

## Installing 

### Install Python
- Note any modern Linux system will already have python 3 installed.

```bash
sudo apt-get install python3 

```

### Install tkinter

`tkinter` is used by `clock` as the grapical interface, you can install with the following command.

```bash

 sudo apt install python3-tk
```

### Install pip

`pip` is a package manager for `python` we will use it to install requirements for `clock`. Sometimes it isn't installed itself.You can install like this.

```bash

 sudo apt install python3-pip
```

### Clone the clock repo
Run this from a terminal in the directory where you want to clone the repository to your local computer

```bash
git clone https://gitlab.com/public_outlaws42/clock.git


```
Change into the project main directory

```bash
cd clock

```

### Install dependencies with pip

```bash
pip3 install -r requirements.txt

```

You will have to make the python files executable.

```bash
chmod u+x clock.py

```

---

## Config

- In the default config I use the **Ubuntu** font. This will not be installed in Rasbian. So do the following to install from the commandline.
```bash
sudo apt install ttf-ubuntu-font-family
 
```

You can edit the config by editing `conf.yaml` in the root of the project.

---

## To run

**clock.py**  This is the main file and is what you would run. 

To run the script from the current directory.

```bash
./clock.py

```

**Note:** You should be able to run this code on Windows and MacOS as well as long as you have the 
prerequisites installed. Running the python files will be different for those platforms but it should be possible.

---

## Raspberry Pi Config


### keep the screen on

From the commandline open this file ``

```bash
sudo nano /etc/lightdm/lightdm.conf
 
```
Find the section `[Seat:*]` in that section find this line `xserver-command=X` it could possibly be commented out so it would look like this `#xserver-command=X` you want to change it to this `xserver-command=X -s 0 dpms` then save.

### Hide the mouse pointer

First you need to install `unclutter` in Debian you can do this with the following command.
```bash
sudo apt-get install unclutter
 
```

Then you will need to add the command to the `pi` user `autostart` file so it will run in the background on reboot. You could possibly have to create this file. I did on Rasbian. to create the dir path and then the file do the following from the commandline 

```bash
mkdir -p ~/.config/lxsession/LXDE-pi/ && touch autostart
 
```

then open the `autostart` file with the following

```bash
nano ~/.config/lxsession/LXDE-pi/autostart
 
```

Then add this line to the `autostart` file

```bash
@unclutter -display :0 -noevents -grab
 
```
Then save and exit the file. You would need to reboot for this to take affect.

---

## Install on a Raspberry pi(kiosk)

Alternatively to install on a raspberry pi where this clock is going to be the only thing displayed. Kind of like a kiosk.  I have included a script. This script is called `pi_setup.py` so you change into the clock directory.

```bash
cd clock

```
Then you would install the requirements.

```bash
pip3 install -r requirements.txt

```
Make the scripts executable
```bash
chmod u+x pi_setup.py clock.py

```
Then you would run the script. After rebooting the clock should start automatically in full screen.
```bash
./pi_setup.py

```

## Features

### Mode
This will display in either full screen mode or a configured size window mode. default size is `640x480`

---

### 24/12
The time can be diplayed in an 12 hour or 24 hour clock.

---

### Status messages

Added the ability to have status messages. The first time `clock.py` is run it will create a file `support/status.yaml` it will add one message for Christmas. Messages can be added to the list one per day in this format.
`date: message` so an example would be `12/25: Merry Christmas`. At this time I am not limiting the length of the message but obviously if you have to long of a message it is not going to look good.  The message will appear right below the date 

---

## License
GPL 2.0