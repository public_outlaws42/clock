#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Raspberry pi setup for kiosk mode.
"""

# Imports
from pathlib import Path
from subprocess import run


# Personal Imports
from lib.colors import Colors
from lib.file_io import (
    update_yaml_single,
    get_resource_path,
    add_path_file,
    add_to_new_file,
    add_symlink,
    copy_file,
)

from lib.inp import reboot

cs = Colors()

__author__ = "Troy Franks"
__version__ = "1.0.6"

home = Path.home()


def update_apt() -> None:
    run(["sudo", "apt", "update"])


def install_yaml() -> None:
    run(["pip3", "install", "pyyaml"])


def install_ubuntu_font() -> None:
    run(["sudo", "apt", "install", "ttf-ubuntu-font-family"])


def update_desktop_db() -> None:
    run(["update-desktop-database", f"{home}/.local/share/applications"])


if __name__ == "__main__":
    print(f"{cs.BOLD}{cs.BLUE}Updating apt repos{cs.END}")
    update_apt()

    print(f"{cs.BOLD}{cs.BLUE}Installing pyyaml{cs.END}")
    install_yaml()

    print(f"{cs.BOLD}{cs.BLUE}Installing Ubuntu font family{cs.END}")
    install_ubuntu_font()

    print(f"{cs.BOLD}{cs.BLUE}Creating autostart file{cs.END}")
    autostart = add_path_file(
        path=f"{home}/.config/lxsession/LXDE-pi",
        file="autostart",
    )

    print(f"{cs.BOLD}{cs.YELLOW}Writing autostart items{cs.END}")
    add_to_new_file(
        file=autostart,
        lines=[
            "@unclutter -display :0 -noevents -grab",
            f"{get_resource_path('clock.py')}",
        ],
    )

    print(f"{cs.BOLD}{cs.BLUE}Copy .desktop to applications{cs.END}")
    dest = copy_file(
        fname="support/clock.desktop",
        destination=f"{home}/.local/share/applications",
        fpath="relative",
        dpath="absolute",
    )

    print(f"{cs.BOLD}{cs.BLUE}Creating link to desktop file{cs.END}")
    autostart = add_symlink(
        link=f"{home}/Desktop/clock",
        fname=f"{dest}/clock.desktop",
        lpath="absolute",
        fpath="absolute",
    )

    print(f"{cs.BOLD}{cs.BLUE}Set fullscreen for raspberry pi{cs.END}")
    update_yaml_single(
        fname="conf.yaml",
        setting="fullscreen",
        status=True,
    )

    # Update desktop database
    print(f"{cs.BOLD}{cs.BLUE}Updating shortcut database{cs.END}")
    update_desktop_db()

    # Reboot computer
    print(
        (
            f"{cs.BOLD}{cs.BLUE}"
            f"\nYou need to reboot for all changes to take affect.{cs.END}"
        )
    )
    reboot()
