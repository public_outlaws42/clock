#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Test ideas for the clock
"""
# Imports
# import tkinter as tk
import ttkbootstrap as ttk

# from ttkbootstrap.constants import *
from datetime import date

# Personal Imports
from lib.file_io import (
    open_yaml,
    update_yaml_single,
    get_resource_path,
)

__author__ = "Troy Franks"
__version__ = "1.0"

# Display Properties
screen = ttk.Window(themename="darkly")
screen.title("Clock")
screen.geometry("640x480")
# screen.config(background="black")
# power = PhotoImage(file=get_resource_path("static/power_48.png"))
# if clock_24 is True:
#     clock_type = PhotoImage(file=get_resource_path("static/24_48.png"))
# else:
#     clock_type = PhotoImage(file=get_resource_path("static/12_48.png"))
fullscreen = True
if fullscreen is True:
    screen.attributes("-fullscreen", True)


global counter
counter = 1


def settings_window():
    if fullscreen is True:
        screen.withdraw()
    global counter
    if counter < 2:
        global settings
        settings = ttk.Toplevel()
        # Full screen configure --------------------------------------
        fs_label = ttk.Label(
            settings,
            text="Check for fullscreen",
        )
        fs_label.grid(
            column=1,
            row=5,
            sticky="w",
            pady=(30, 0),
            padx=(30, 0),
        )
        fs_var = ttk.BooleanVar()
        fs_var.set(fullscreen)
        check_fs = ttk.Checkbutton(
            settings,
            variable=fs_var,
        )  # onvalue="RGB", offvalue="YCbCr"
        check_fs.grid(
            column=1,
            row=5,
            sticky="w",
            pady=(30, 0),
            padx=(10, 0),
        )

        # settings.attributes("-topmost", True)
        # settings.grab_set()
        settings.title("Settings Page")
        # settings.focus()
        settings.lift()
        settings.geometry("640x480")
        # settings.minsize(640, 480)
        if fullscreen is True:
            # screen.withdraw()
            settings.attributes("-fullscreen", True)

        # font_entry = ttk.Entry(settings, bootstyle="success").pack(pady=20)

        ok_button = ttk.Button(
            settings,
            text="Ok",
            command=lambda btn="ok_button": ok_cancel(btn),
        )
        ok_button.grid(
            column=2,
            row=10,
            pady=(40, 0),
            padx=(195, 0),
        )
        cancel_button = ttk.Button(
            settings,
            text="Cancel",
            command=lambda btn="cancel_button": ok_cancel(btn),
        )
        cancel_button.grid(
            column=2,
            row=10,
            pady=(40, 0),
            padx=(20, 0),
        )

        def ok_cancel(btn):
            if btn == "cancel_button":
                settings.destroy()
                print(btn)
            if btn == "ok_button":
                # zip_ = self.zip_var.get()
                # icon = self.icon_var.get()
                # ip = self.ip_var.get()
                # api = self.api_var.get()
                fs = fs_var.get()
                # ms = self.ms_var.get()
                # fc = self.fc_var.get()
                # pst = self.pst_var.get()

                # output = {'code': zip_, 'icon_path': icon,
                #           'broker_add': ip, 'api': api, 'fullscreen': fs,
                #           'unit': ms, 'forecast_source': fc, 'temp_past': pst}
                # self.write_config('config.json', output)
                print(fs)
                settings.destroy()
            if fullscreen is True:
                screen.deiconify()  # returns root window when done with settings dialog

        counter += 1


def ok_cancel(btn):
    if btn == "cancel_button":
        settings.destroy()
        print(btn)
    if btn == "ok_button":
        # zip_ = self.zip_var.get()
        # icon = self.icon_var.get()
        # ip = self.ip_var.get()
        # api = self.api_var.get()
        # fs = self.fs_var.get()
        # ms = self.ms_var.get()
        # fc = self.fc_var.get()
        # pst = self.pst_var.get()

        # output = {'code': zip_, 'icon_path': icon,
        #           'broker_add': ip, 'api': api, 'fullscreen': fs,
        #           'unit': ms, 'forecast_source': fc, 'temp_past': pst}
        # self.write_config('config.json', output)
        settings.destroy()

    if fullscreen is True:
        screen.deiconify()  # returns root window when done with settings dialog


settings = ttk.Button(
    screen,
    text="settings",
    command=settings_window,
).pack(pady=40)
close = ttk.Button(
    screen,
    text="Close",
    command=screen.destroy,
).pack(pady=20)

if __name__ == "__main__":
    screen.mainloop()
