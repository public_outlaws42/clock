#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Full screen clock for display on a 5" screen
"""

# Imports
from subprocess import run

# Personal Imports
from lib.colors import Colors

cs = Colors()

__author__ = "Troy Franks"
__version__ = "1.0.5"


def get_input_yes_no(
    in_message: str,
    default: str = "",
) -> bool:
    while True:
        item = (
            input(
                (
                    f"{cs.BOLD}{in_message} (yes or no): {cs.GREEN}(Default: "
                    f"{default}): {cs.END}"
                )
            ).strip()
            or default
        )
        if item.lower() == "yes":
            return True
        elif item.lower() == "no":
            return False
        else:
            print(
                (
                    f"{cs.RED}{cs.BOLD}"
                    f"You need to either enter yes or no{cs.END}\n"
                )
            )


def reboot() -> None:
    answer = get_input_yes_no(
        in_message="\nDo you want to reboot now?",
        default="no",
    )
    if answer is True:
        print((f"{cs.RED}{cs.BOLD}" f"Going to reboot now{cs.END}\n"))
        run(["sudo", "reboot", "-h", "now"])
    else:
        print(
            (
                f"{cs.RED}{cs.BOLD}"
                f"Make sure to reboot at some point{cs.END}\n"
            )
        )
