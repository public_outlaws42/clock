#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions for dealing with files
"""

# Imports
from yaml import safe_dump, safe_load
from pathlib import Path
from shutil import copy

# Personal Imports
from lib.colors import Colors

cs = Colors()

__author__ = "Troy Franks"
__version__ = "1.0.6"


def get_resource_path(rel_path) -> Path:
    """
    returns full path of one level up the relative path provided
    requires the pathlib module
    """
    dir_of_py_file = Path(__file__).parent.parent.resolve()
    abs_path_to_resource = dir_of_py_file.joinpath(rel_path)
    return abs_path_to_resource


def add_path_file(
    path: str,
    file: str,
) -> str:
    full_path: Path = Path(f"{path}")
    str_path = full_path.resolve()
    if full_path.exists() is False:
        str_path.mkdir(parents=True)
        Path(f"{str_path}/{file}").touch()
    else:
        print(f"{str_path}/{file} {cs.RED}already exists{cs.END}")
    return f"{str_path}/{file}"


def add_to_new_file(
    file: str,
    lines: list,
) -> None:
    file_stat = Path(file).stat()
    print(f"file_size: {file_stat.st_size}")
    if file_stat.st_size == 0:
        with open(f"{file}", "w") as f:
            for i in lines:
                f.writelines(f"{i}\n")
    else:
        print(f"{file} {cs.RED}Items already exist{cs.END}")


def open_yaml(
    fname: str,
    def_content: dict = {"key": "value"},
):
    try:
        with open(get_resource_path(fname), "r") as fle:
            content = safe_load(fle)
        return content
    except (FileNotFoundError, EOFError) as e:
        print(e)
        with open(get_resource_path(fname), "w") as output:
            safe_dump(def_content, output, sort_keys=True)
        return def_content


def update_yaml_single(
    fname: str,
    setting: str,
    status: bool,
) -> None:
    with open(get_resource_path(fname), "r") as file:
        content = safe_load(file)

    content[setting] = status

    with open(get_resource_path(fname), "w") as file:
        safe_dump(content, file)


def add_symlink(
    link: str,
    fname: str,
    fpath: str = "absolute",
    lpath: str = "absolute",
) -> None:
    """
    fname = filename and path,
    fpath = path type for file(relative, absolute),
    lpath = path type for link(ralative, absolute),
    Creates a symbolic link for fname
    """

    target_file = (
        Path(f"{fname}")
        if fpath.lower() == "absolute"
        else Path(f"{get_resource_path(fname)}")
    )
    target_link = (
        Path(f"{link}")
        if lpath.lower() == "absolute"
        else Path(f"{get_resource_path(link)}")
    )
    if target_file.is_file() is True and target_link.is_symlink() is False:
        target_link.symlink_to(target_file)
        print(f"{cs.YELLOW}Symlink created{cs.END}")
    else:
        print(
            (
                f"{target_file.resolve()} {cs.RED}Doesn't exist or{cs.END}"
                f" {target_link} {cs.RED}exists already{cs.END}"
            )
        )


def copy_file(
    fname: str,
    destination: str,
    fpath: str = "relative",
    dpath: str = "relative",
) -> Path:
    """
    fname = filename and path,
    destination = the destination directory,
    fpath = path type for file(relative, absolute),
    dpath = path type for destination(ralative, absolute),
    Copies fname to the destination if fname and
    destination exists. returns destination dir.
    """
    cp_file = (
        Path(f"{fname}")
        if fpath.lower() == "absolute"
        else Path(f"{get_resource_path(fname)}")
    )
    dest_dir = (
        Path(f"{destination}")
        if dpath.lower() == "absolute"
        else Path(f"{get_resource_path(destination)}")
    )
    if cp_file.is_file() is True and dest_dir.is_dir() is True:
        copy(cp_file, dest_dir)
    else:
        print(
            (
                f"{dest_dir.resolve()} {cs.RED}Doesn't exist or{cs.END}"
                f" {cp_file.resolve()} {cs.RED}doesn't exist{cs.END}"
            )
        )
    return dest_dir.resolve()
