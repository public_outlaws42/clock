#! /usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Assign colors for commandline use.
"""

__author__ = "Troy Franks"
__version__ = "2023-05-12"


class Colors:
    PURPLE = "\033[95m"
    CYAN = "\033[96m"
    DARKCYAN = "\033[36m"
    BLUE = "\033[94m"
    GREEN = "\033[92m"
    YELLOW = "\033[93m"
    RED = "\033[91m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    END = "\033[0m"

    # Header colored text functions

    def prRedMulti(self, ntext, text):
        print(f"{ntext}\033[91m\033[1m{text}\033[00m")

    def prCyanMulti(self, ntext, text):
        print(f"{ntext}\033[96m {text}\033[00m")

    def prCyanMultiB(self, ntext, text):
        print(f"{ntext}\033[96m\033[1m{text}\033[00m")

    def prRedBold(self, text):
        print(f"\033[91m\033[1m{text}\033[00m")

    def prGreenBold(self, text):
        print(f"\033[92m\033[1m{text}\033[00m")

    def prYellowBold(self, text):
        print(f"\033[93m\033[1m{text}\033[00m")

    def prLightPurpleBold(self, text):
        print(f"\033[94m\033[1m{text}\033[00m")

    def prPurpleBold(self, text):
        print(f"\033[95m\033[1m{text}\033[00m")

    def prCyanBold(self, text):
        print(f"\033[96m\033[1m{text}\033[00m")

    def prLightGrayBold(self, text):
        print(f"\033[97m\033[1m{text}\033[00m")

    def prBlackBold(self, text):
        print(f"\033[98m\033[1m{text}\033[00m")

    def prRed(self, text):
        print(f"\033[91m{text}\033[00m")

    def prGreen(self, text):
        print(f"\033[92m{text}\033[00m")

    def prYellow(self, text):
        print(f"\033[93m{text}\033[00m")

    def prLightPurple(self, text):
        print(f"\033[94m{text}\033[00m")

    def prPurple(self, text):
        print(f"\033[95m{text}\033[00m")

    def prCyan(self, text):
        print(f"\033[96m{text}\033[00m")

    def prLightGray(self, text):
        print(f"\033[97m{text}\033[00m")

    def prBlack(self, text):
        print(f"\033[98m{text}\033[00m")

    # Inline colored text functions bold

    def green_bold(self, text):
        return f"\033[92m\033[1m{text}\033[00m"

    def red_bold(self, text):
        return f"\033[91m\033[1m{text}\033[00m"

    def purple_bold(self, text):
        return f"\033[95m\033[1m{text}\033[00m"

    def cyan_bold(self, text):
        return f"\033[96m\033[1m{text}\033[00m"

    def yellow_bold(self, text):
        return f"\033[93m\033[1m{text}\033[00m"

    # Inline colored text functions

    def red(self, text):
        return f"\033[91m{text}\033[00m"

    def cyan(self, text):
        return f"\033[96m{text}\033[00m"

    def green(self, text):
        return f"\033[92m{text}\033[00m"

    def purple(self, text):
        return f"\033[95m{text}\033[00m"
