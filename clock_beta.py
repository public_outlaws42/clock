#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Full screen clock for display on a 5" screen
"""

# Imports
import subprocess as sp

# from yaml import safe_dump, safe_load
from tkinter import Tk, Label, Button, PhotoImage, messagebox
import ttkbootstrap as ttk
from time import strftime
from datetime import date

# Personal Imports
from lib.file_io import (
    open_yaml,
    update_yaml_single,
    get_resource_path,
)

__author__ = "Troy Franks"
__version__ = "1.0.8"

# Global Variables

settings = open_yaml(fname="conf.yaml")
display_font: str = settings["display_font"]
font_size_clock: int = settings["font_size_clock"]
font_size_date: int = settings["font_size_date"]
font_size_status: int = settings["font_size_status"]
background_color: str = settings["background_color"]
foreground_color: str = settings["foreground_color"]
clock_24: bool = settings["clock_24"]
fullscreen: bool = settings["fullscreen"]


# Display Properties
screen = ttk.Window(themename="darkly")
screen.title("Clock")
screen.geometry("640x480")
screen.config(background=background_color)
power = PhotoImage(file=get_resource_path("static/power_48.png"))
if clock_24 is True:
    clock_type = PhotoImage(file=get_resource_path("static/24_48.png"))
else:
    clock_type = PhotoImage(file=get_resource_path("static/12_48.png"))

if fullscreen is True:
    screen.attributes("-fullscreen", True)
    # screen.overrideredirect(True)  # Make the window borderless


def close():
    lxde_logout = sp.run(
        ["which", "lxde-logout"],
        stdout=sp.PIPE,
        stderr=sp.PIPE,
    )
    if lxde_logout.returncode == 0:
        sp.run(["lxde-logout"])
    else:
        close_warning()


def close_warning():
    response = messagebox.askokcancel(
        "Warning", "Are you sure you want to close"
    )
    if response is True:
        screen.destroy()


def get_time() -> None:
    if clock_24 is True:
        str_time = strftime("%H:%M")
    else:
        str_time = strftime("%I:%M")
    clock.config(text=str_time)
    clock.after(1000, get_time)


def get_date() -> None:
    str_date = date.today().strftime("%a %b %d %Y")
    date_today.config(text=str_date)
    date_today.after(3600000, get_date)  # 3600000 1 hour, 60000 1 min


def get_status() -> None:
    message: str = ""
    status_file = open_yaml(
        fname="support/status.yaml",
        def_content={"12/25": "Merry Christmas!"},
    )
    try:
        today_date = date.today().strftime("%m/%d")
        message: str = status_file[f"{today_date}"]
    except KeyError:
        pass
    status.config(text=message)
    status.after(3600000, get_status)


def clock_status():
    global clock_24
    global clock_type
    clock_24 = not clock_24

    update_yaml_single(
        fname="conf.yaml",
        setting="clock_24",
        status=clock_24,
    )
    # Refresh clock status icon
    if clock_24 is True:
        clock_type = PhotoImage(file=get_resource_path("static/24_48.png"))
    else:
        clock_type = PhotoImage(file=get_resource_path("static/12_48.png"))
    toggle.config(image=clock_type)


clock: Label = Label(
    screen,
    font=(display_font, font_size_clock),
    background=background_color,
    foreground=foreground_color,
)
clock.place(relx=0.5, rely=0.5, anchor="center")

date_today: Label = Label(
    screen,
    font=(display_font, font_size_date),
    background=background_color,
    foreground=foreground_color,
)
date_today.place(relx=0.5, rely=0.75, anchor="center")

status: Label = Label(
    screen,
    font=(display_font, font_size_status),
    background=background_color,
    foreground=foreground_color,
)
status.place(relx=0.5, rely=0.90, anchor="center")

# if fullscreen is True:
exit: Button = Button(
    screen,
    font=(display_font, 8),
    image=power,
    bd=0,
    highlightthickness=0,
    background=background_color,
    foreground=background_color,
    command=close,
)
exit.place(relx=0.55, rely=0.15, anchor="center")

toggle: Button = Button(
    screen,
    font=(display_font, 8),
    image=clock_type,
    bd=0,
    highlightthickness=0,
    background=background_color,
    foreground=background_color,
    command=clock_status,
)
toggle.place(relx=0.4, rely=0.15, anchor="center")

get_time()
get_date()
get_status()

if __name__ == "__main__":
    screen.mainloop()
